;;; init.el --- Load the full configuration -*- lexical-binding: t -*-
;;; Commentary:


;;; init.el --- GNU Emacs Configuration


;;

;;; Commentary:

;; Following lines build the configuration code out of the custom.el file.

;;; Code:

;; Make startup faster by reducing the frequency of garbage
;; collection.
(setq gc-cons-threshold (* 50 1000 1000))

(require 'package)
(package-initialize)

(if (file-exists-p (expand-file-name "custom.el" user-emacs-directory))
    (load-file (expand-file-name "custom.el" user-emacs-directory))
  (org-babel-load-file (expand-file-name "custom.org" user-emacs-directory)))

;; Make gc pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))



;; Local Variables:
;; coding: utf-8
;; no-byte-compile: t
;; End:
;;; init.el ends here
